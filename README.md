# TaskEasy

TaskEasy CODING CHALLENGE
Due date: May Monday 6th 2019
Use any language to write a program that achieves desired result
Send us the code, the input, and the output 


Given: 
Kyle has meetings at 1:30PM, 2:30PM, and 6PM
Paul has meetings at 7AM, 9AM, 1:30PM, 3PM, and 3:30PM
Alex has meetings at 8AM, 9:30AM, 12:30PM, and 3PM
Luis has meetings at 9AM, 1:30PM, 3PM, and 3:30PM
Jairo has meetings at 8AM, 9AM, and 6PM
Sonya has a meeting at 8AM, 12:30PM, 1:30PM, 3:30PM
Office hours are 8AM-5PM
Lunch is 12PM-1PM
All meetings are half an hour long
Desired Result:
Return all the times when at least three people are available and who those people are
Result should be in a format that can be used as an input for another program


web: http://theapparchitect.net/taskeasy/
mobile: see video in repo